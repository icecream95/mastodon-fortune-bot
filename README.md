# Mastodon Fortune Of The Day bot

This bot prints a fortune every day on Mastodon. The original bot is at
[@fotd@mastodon.xyz](https://mastodon.xyz/@fotd).

How to install:

1. Fork mastodon-fortune-bot at https://gitlab.com/icecream95/mastodon-fortune-bot/forks/new.
2. Go into Settings -> CI / CD in your project.
3. Install [toot](https://github.com/ihabunek/toot/blob/master/README.rst) on your local computer. You can remove this afterwards.
4. Type `toot login` on your computer to login to your bot.
5. Expand the **Secret variables** section and add a variable called `TOOT_INSTANCE` with the URL of your bot's Mastodon instance (e.g. `mastodon.xyz`), `TOOT_INSTANCE_INFO`, which should be the contents of `~/.config/toot/instances/<instance name>`, with newlines replaced with \n. Do the same for `TOOT_LOGIN_INFORMATION`, from `~/.config/toot/user.cfg`,
6. Edit run.sh to your linkng.
7. Go into CI / CD -> Schedules and add a new schedule for your bot to automatically run.
8. Don't forget to add a variable called DO_TOOT and set it to 1.
9. Share the news on your primary Mastodon account.
10. You are now done! 