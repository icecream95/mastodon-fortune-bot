TRIES=
FORTUNE="$(/usr/games/fortune || echo)"
echo -n "$(echo $FORTUNE | wc -c): "
echo $FORTUNE
while [ $(echo $FORTUNE | wc -c) -gt 490 ] || [ $(echo $FORTUNE | wc -c) -lt 5 ]; do
  TRIES+="x"
  if [ $(echo $TRIES | wc -c) -gt 20 ]; then
    # Fortune isn't working properly; quit
    echo Sorry, nothing today, but check back tomorrow. | tee -a /dev/stderr | toot post
    exit 1
  fi
  sleep 1
  FORTUNE="$(/usr/games/fortune || echo)"
  echo -n "$(echo $FORTUNE | wc -c): "
  echo $FORTUNE
done
echo $FORTUNE | tee -a /dev/stderr | toot post
